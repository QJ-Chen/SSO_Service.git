package com.qjchen.server.mapper;

import com.qjchen.common.api.mapper.BusinessMapper;
import com.qjchen.common.model.dto.AppDTO;
import com.qjchen.common.model.entity.App;
import org.springframework.stereotype.Repository;

@Repository
public interface AppMapper extends BusinessMapper<App, AppDTO> {
}
