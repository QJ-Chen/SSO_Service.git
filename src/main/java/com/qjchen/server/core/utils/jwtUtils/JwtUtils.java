package com.qjchen.server.core.utils.jwtUtils;

import com.alibaba.fastjson.JSONObject;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
public class JwtUtils {

    private static final Logger log = LoggerFactory.getLogger(JwtUtils.class);

   // 设置token过期时间
    public static final Integer EXPIRE = 1000 * 60 * 60 * 24;
    //密钥，随便写，做加密操作
    public static final String APP_SECRET ="xbrceXUKwYIRoQJndTPFNzAmhDagkLMExbrceXUKwYIRoQJndTPFNzAmhDagkLME";

    public static final String USER_KEY = "userToken";

    //生成jwt字符串的方法
    public static String getJwtToken(Integer id, String nickname){
        try {
            // 私钥和加密算法
            Algorithm algorithm = Algorithm.HMAC256(APP_SECRET);
            // 设置头部信息
            Map<String, Object> header = new HashMap<>(2);
            header.put("Type", "Jwt");
            header.put("alg", "HS256");

            return JWT.create()
                    .withHeader(header)
                    .withClaim("token", JSONObject.toJSONString(id+nickname))
                    .withExpiresAt(new Date())
                    .sign(algorithm);
        } catch (Exception e) {
            log.error("generate token occur error, error is:{}", e);
            throw new RuntimeException();
        }
    }
}
