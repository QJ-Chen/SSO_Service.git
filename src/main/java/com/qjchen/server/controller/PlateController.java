package com.qjchen.server.controller;

import com.qjchen.common.api.controller.BusinessController;
import com.qjchen.common.model.Page;
import com.qjchen.common.model.dto.PlateDTO;
import com.qjchen.common.model.entity.Plate;
import com.qjchen.common.model.vo.PlateVO;
import com.qjchen.common.utils.ConvertUtil;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("${qjc.sso.api}/plate")
public class PlateController extends BusinessController<PlateVO, PlateDTO,Plate> {

    @Override
    public Page<PlateVO> listPage(@RequestBody PlateDTO plateDTO) {
        int currentPageNo = plateDTO.getCurrentPageNo();
        if (currentPageNo!=0){
            plateDTO.setCurrentPageNo(plateDTO.getPageSize()*plateDTO.getCurrentPageNo());
        }

        List<PlateVO> plateVOS = new ArrayList<>();
        super.serviceAdapter.list(plateDTO).forEach(plateDTOTemp -> {
            PlateVO fusion = ConvertUtil.fusion(plateDTOTemp, PlateVO.class);
            fusion.setPlateType(plateDTOTemp.getPlateType());
            fusion.setStatus(plateDTOTemp.getStatus());

            plateVOS.add(fusion);
        });

        int count = super.serviceAdapter.count(plateDTO);

        return new Page<>(plateVOS.size()/plateDTO.getPageSize(),currentPageNo,plateDTO.getPageSize(),count,plateVOS);
    }

    @Override
    public PlateVO get(@RequestBody PlateDTO plateDTO) {
        PlateDTO plateDTOTemp = super.serviceAdapter.get(plateDTO);
        PlateVO plateVO = ConvertUtil.fusion(plateDTOTemp, PlateVO.class);
        plateVO.setStatus(plateDTOTemp.getStatus());
        return plateVO;
    }
}
