package com.qjchen.server.interceptor;

import com.qjchen.common.config.RedisConstants;
import com.qjchen.common.model.annotations.ExclusionAuthority;
import com.qjchen.server.core.utils.redisUtils.RedisPoolUtil;
import com.qjchen.server.model.exception.AppException;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.Map;

public class ServiceAppHandler implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler){
        if (handler instanceof HandlerMethod){
            HandlerMethod handlerMethod = (HandlerMethod) handler;
            Method method = handlerMethod.getMethod();
            if (method.isAnnotationPresent(ExclusionAuthority.class)){
                return true;
            }
        }
        String appToken = request.getHeader("appToken")!=null?request.getHeader("appToken"):request.getParameter("appToken");
        String appCode = request.getHeader("appCode")!=null?request.getHeader("appCode"):request.getParameter("appCode");
        if (!RedisPoolUtil.heixists(RedisConstants.APP_TICKET_REDIS_TEMP, "appTicket_" + appCode)){
            throw new AppException("######  非法访问统一认证平台 ###### ");
        }
        Map appMap = RedisPoolUtil.hget(RedisConstants.APP_TICKET_REDIS_TEMP, "appTicket_" + appCode, Map.class);
        if (appToken.equals(appMap.get("appToken"))){
            return true;
        }
        throw new AppException("######  非法访问统一认证平台 ###### ");
    }
}
