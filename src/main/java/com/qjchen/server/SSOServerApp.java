package com.qjchen.server;

import com.qjchen.common.interceptor.ResponseAdvice;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@MapperScan("com.qjchen.server.mapper")
@ComponentScan(basePackages = {"com.qjchen"})
@SpringBootApplication
public class SSOServerApp extends SpringBootServletInitializer {

    /**
     * 用于打包war包，可部署至外部tomcat/jetty等容器中
     * @param builder a builder for the application context
     * @return SpringApplicationBuilder
     */
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(SSOServerApp.class);
    }

    public static void main(String[] args) {
        ConfigurableApplicationContext run = SpringApplication.run(SSOServerApp.class,args);
    }

    @Bean
    public ResponseAdvice setResponseAdvice(){
        return new ResponseAdvice();
    }
}
