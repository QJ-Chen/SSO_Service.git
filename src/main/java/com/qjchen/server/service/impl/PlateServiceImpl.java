package com.qjchen.server.service.impl;

import com.qjchen.common.api.service.BusinessServiceAdapter;
import com.qjchen.common.model.dto.PlateDTO;
import com.qjchen.common.model.entity.Plate;
import com.qjchen.server.service.PlateService;
import org.springframework.stereotype.Service;

@Service
public class PlateServiceImpl extends BusinessServiceAdapter<Plate, PlateDTO> implements PlateService {

}
