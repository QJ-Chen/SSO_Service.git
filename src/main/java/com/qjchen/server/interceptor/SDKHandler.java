package com.qjchen.server.interceptor;

import com.qjchen.common.model.annotations.ExclusionAuthority;
import com.qjchen.server.model.annotation.SDK;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;

public class SDKHandler implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (handler instanceof HandlerMethod){
            HandlerMethod handlerMethod = (HandlerMethod) handler;
            Method method = handlerMethod.getMethod();
            if (method.isAnnotationPresent(SDK.class)){
                return true;
            }
        }
        String appToken = request.getHeader("appToken")!=null?request.getHeader("appToken"):request.getParameter("appToken");
        String appCode = request.getHeader("appCode")!=null?request.getHeader("appCode"):request.getParameter("appCode");
        return true;
    }
}
