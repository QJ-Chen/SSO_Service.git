package com.qjchen.server.service;

import com.qjchen.common.api.service.BusinessService;
import com.qjchen.common.model.dto.AppDTO;

public interface AppService extends BusinessService<AppDTO> {
}
