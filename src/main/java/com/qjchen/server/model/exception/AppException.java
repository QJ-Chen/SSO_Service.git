package com.qjchen.server.model.exception;

public class AppException extends RuntimeException{
    public AppException(String message) {
        super(message);
    }
}
