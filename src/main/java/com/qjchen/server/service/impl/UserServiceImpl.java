package com.qjchen.server.service.impl;

import com.qjchen.common.api.service.BusinessServiceAdapter;
import com.qjchen.common.model.dto.UserDTO;
import com.qjchen.common.model.entity.User;
import com.qjchen.server.service.UserService;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl extends BusinessServiceAdapter<User,UserDTO> implements UserService {

}
