package com.qjchen.server.model.annotation;

import java.lang.annotation.*;

@Inherited
@Documented
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface SDK {
    String value() default "";
}
