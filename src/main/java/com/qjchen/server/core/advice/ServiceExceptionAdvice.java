package com.qjchen.server.core.advice;

import com.qjchen.server.model.exception.AppException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class ServiceExceptionAdvice{

    @ExceptionHandler({AppException.class})
    public ModelAndView handleAppExceptionException(AppException exception) {
        ModelAndView modelAndView = new ModelAndView("appException");
        modelAndView.addObject("msg", exception.getMessage());
        return modelAndView;
    }
}
